let posts = [];
let count = 1;

// adding post data

document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	e.preventDefault();
	posts.push({
		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value,
	})

	count++;

	showPosts(posts);
	alert('Successfully added post!')
})

// showPosts()
const showPosts = (post) => {
	let postEntries = '';
	post.forEach((post) => {
		postEntries += `
		<div id="post-${post.id}">
			<h3 id="post-title-${post.id}">${post.title}</h3>
			<p id="post-body-${post.id}">${post.body}</p>
			<button onclick="editPost(${post.id})">Edit</button>
			<button onclick="deletePost(${post.id})">Delete</button>
		</div>
		`
	})
	console.log(post)
	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

/*to create an editPost function that lets us transfer the id, title and body of each post entry to the second form
        when the edit button has been clicked:
            the id of the div will be copied to the hidden input field of Edit Post form
            the title of the div will be copied to the title input field of Edit Post form
            the body of the div will be copied to the body input field of Edit Post form*/
const editPost = (id) => {
	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector('#txt-edit-id').value = id;
	document.querySelector('#txt-edit-title').value = title;
	document.querySelector('#txt-edit-body').value = body;

}

document.querySelector('#form-edit-post').addEventListener("submit", (e) => {
	e.preventDefault();

	for (let i=0; i<posts.length; i++) {
		if (posts[i].id.toString() === document.querySelector('#txt-edit-id').value) {
			posts[i].title = document.querySelector('#txt-edit-title').value;
			posts[i].body = document.querySelector('#txt-edit-body').value;
			
			showPosts(posts);
			alert('Successfully Updated!');
			break;
		}
	}
})

const deletePost = (id) => {
	posts.splice(id-1, 1);
	showPosts(posts)
}